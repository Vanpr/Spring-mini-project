package com.example.demo.services;

import com.example.demo.models.Category;

import java.util.List;

public interface CategoryService  {
    List<Category> getAll();

    Integer count();
}
