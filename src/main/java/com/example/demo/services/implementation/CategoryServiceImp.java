package com.example.demo.services.implementation;

import com.example.demo.models.Category;
import com.example.demo.repositeries.providers.CategoryRepositories;
import com.example.demo.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImp implements CategoryService {

    CategoryRepositories categoryRepositories;

    @Autowired
    public CategoryServiceImp(CategoryRepositories categoryRepositories) {
        this.categoryRepositories = categoryRepositories;
    }

    @Override
    public List<Category> getAll() {
        return this.categoryRepositories.getAll();
    }

    @Override
    public Integer count() {
        return this.categoryRepositories.count();
    }
}
