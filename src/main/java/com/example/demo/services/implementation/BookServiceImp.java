package com.example.demo.services.implementation;

import com.example.demo.models.Book;
import com.example.demo.repositeries.BookRepositories;
import com.example.demo.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImp implements BookService {

    private BookRepositories bookRepositories;

    @Autowired
    public BookServiceImp(BookRepositories bookRepositeries) {
        this.bookRepositories = bookRepositeries;
    }


    @Override
    public List<Book> getAll() {
        return this.bookRepositories.getAll();
    }

    @Override
    public Book findOne(Integer id) {
        return  this.bookRepositories.findOne(id);
    }

    @Override
    public boolean update(Book book) {
        return this.bookRepositories.update(book);
    }

    @Override
    public boolean remove(Integer id) {
        return  this.bookRepositories.remove(id);
    }

    @Override
    public Integer count() {
        return this.bookRepositories.count();
    }

    @Override
    public boolean create(Book book) {
        return this.bookRepositories.create(book);
    }


}
