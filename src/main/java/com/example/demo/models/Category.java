package com.example.demo.models;

import com.example.demo.models.Book;

import java.util.List;

public class Category {

    private Integer id;
    private String category;

    List<Book> books;


    public Category() {
    }

    public Category(Integer id, String name, List<Book> books) {
        this.id = id;
        this.category = name;
        this.books = books;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return category;
    }

    public void setName(String name) {
        this.category = name;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;

    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", name='" + category + '\'' +
                ", books=" + books +
                '}';
    }
}
