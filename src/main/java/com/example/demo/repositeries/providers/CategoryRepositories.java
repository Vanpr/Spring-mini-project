package com.example.demo.repositeries.providers;

import com.example.demo.models.Category;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepositories {
    @Select("select * from tb_category order by cate_id")
    List<Category> getAll();

    @Select("select count(*) from tb_category")
    Integer count();
}
