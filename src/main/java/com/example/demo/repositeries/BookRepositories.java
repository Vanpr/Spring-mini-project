package com.example.demo.repositeries;

import com.example.demo.models.Book;
import com.example.demo.repositeries.providers.BookProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepositories {
    @SelectProvider(type = BookProvider.class, method = "getAllProvider")
//    @Results({
//            @Result(column = "id", property = "id"),
//            @Result(column = "cate_id", property = "category.id"),
//            @Result(column = "name", property = "category.name")
//    })
    List<Book> getAll();


    @Select("select * from tb_book b INNER JOIN tb_category c ON b.cate_id = c.cate_id where b.id=#{id}")
    Book findOne(@Param("id") Integer id);


    @Update("update tb_book b set title=#{title}, author=#{author}, publisher=#{publisher} where id=#{id}")
    boolean update(Book book);

    @Delete("delete from tb_book where id=#{id}")
    boolean remove(Integer id);



    @Select("select count(*) from tb_book")
    Integer count();


    @InsertProvider(type = BookProvider.class, method = "create")
    boolean create(Book book);

}
