package com.example.demo.Controller;

import com.example.demo.models.Book;
import com.example.demo.models.Category;
import com.example.demo.services.BookService;
import com.example.demo.services.CategoryService;
import com.example.demo.services.UploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Controller
public class BookController {

    private BookService bookService;

    @Autowired
    private CategoryService categoryService;

    private UploadService uploadService;


    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @RequestMapping(method = RequestMethod.GET, value = {"/index", "/", "/home"})
    public String index(ModelMap map) {

        List<Book> bookList = this.bookService.getAll();

        map.addAttribute("books",bookList);
        System.out.println(bookList);


        return "book/index";
    }

    @GetMapping("/view/{id}")
    public String view(@PathVariable("id") Integer id,ModelMap map){

        Book book = this.bookService.findOne(id);
        map.addAttribute("book",book);

        return  "book/view-detail";
    }

    @GetMapping("/update/{id}")
    public String showUpdate(@PathVariable Integer id,ModelMap map){

        Book book = this.bookService.findOne(id);
        map.addAttribute("book",book);
        System.out.println(book);

        return "book/update";
    }

    @PostMapping("update/submit")
    public String updateSubmit(@ModelAttribute Book book){
        System.out.println(book);
        this.bookService.update(book);
//        System.out.println("hello");
        return  "redirect:/index";
    }

    @GetMapping("remove/{id}")
    public String remove(@PathVariable Integer id) {
        this.bookService.remove(id);

        return "redirect:/";
    }

    @GetMapping("/count")
    @ResponseBody
    public Map <String,Object> count(){
        Map<String,Object> response = new HashMap<>();

        response.put("record_count",this.bookService.count());
        response.put("status",true);

        return response;
    }

    @GetMapping("/create")
    public String create(Model model){

        model.addAttribute("book",new Book());
        return "book/create-book";
    }

//    @PostMapping("/create/submit")
//    public String createSubmit(@Valid Book book, BindingResult bindingResult){
//        if(bindingResult.hasErrors()){
//            return  "book/create-book";
//        }
//
//        this.bookService.create(book);
//
//        return  "redirect:/home";
//    }

    @PostMapping("/create/submit")
    public String createSubmit(@Valid Book book, BindingResult bindingResult, MultipartFile file) {
        System.out.println(book);

//        if (bindingResult.hasErrors()) {
//            model.addAttribute("isNew", true);
//
//
//            List<Category> categories = this.categoryService.getAll();
//            model.addAttribute("categories", categories);
//
//            return "book/add-book";
//        }

//        String filename = this.uploadService.upload(file, "pp/");
//
//
//        book.setThumbnail(filename);


//        System.out.println(book.);
        this.bookService.create(book);

        return "redirect:/index";
    }

    @GetMapping("/test")
    public String test(){

        return  "index";

    }

}
